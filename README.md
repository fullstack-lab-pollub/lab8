W tym zadaniu należało utworzyć:
- przestrzeń nazw restricted


![](/Lab8_1.png)


- serwer WWW o nazwie lab8server w przestrzeni nazw restricted używając obraz nginx


![](/Lab8_2.png)


- utworzyć do tego serwera WWW Service


![](/Lab8_3.png)


- utworzyć Pod Sleepybox1


![](/Lab8_4.png)


- uwtorzyć Pod Sleepybox2


![](/Lab8_5.png)


- utworzyć NetworkPolicy, które ogranicza ruch przychodzący do przestrzeni nazw restricted w taki sposób, że
dostęp do lab8server ma tylko Sleepybox1 z domyślnej przestrzeni nazw, a każdy inny dostęp do przestrzeni restricted jest zabroniony


![](/Lab8_6.png)


Poniżej uruchomiono utworzone pliki yaml:


![](/Lab8_7.png)


Następnie sprawdzono poprawność uruchomienia serwera WWW, Service dla przestrzeni nazw restricted:


![](/Lab8_8.png)


Później sprawdzono NetworkPolicy w przestrzeni nazw restricted i podów Sleepybox1 i Sleepybox2 w domyślnej przestrzeni nazw:


![](/Lab8_9.png)


Ostatecznie sprawdzono ograniczenie ruchu:


![](/Lab8_10.png)


Niestety pomimo poprawnego utworzonia NetworkPolicy - ruch do Sleepybox2 nie został ograniczony. Być może jest to jakiś problem
powiązany z Calico.
